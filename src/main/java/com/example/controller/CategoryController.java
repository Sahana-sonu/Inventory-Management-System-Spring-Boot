package com.example.controller;

import com.example.entity.TheLogConverter;
import com.example.entity.Category;
import com.example.service.CategoryLogService;
import com.example.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    @Autowired
    public CategoryService categoryService;
    @Autowired
    private CategoryLogService categoryLogService;

    @GetMapping
    public Iterable<Category> getAllCategory() {
        return categoryService.findAll();
    }

    @GetMapping("/{categoryId}")
    public Optional<Category> searchCategory(@PathVariable("categoryId") int categoryId) {
        return categoryService.findById(categoryId);
    }

    @PostMapping
    public void addCategory(@RequestBody Category category) {
        categoryService.insert(category);
        categoryLogService.insert(TheLogConverter.categoryLogConverter(category));
    }

    @PutMapping("/{categoryId}")
    public void updateCategory(@RequestBody Category category, @PathVariable("categoryId") int categoryId) {
        categoryService.updateCategory(category);
        categoryLogService.insert(TheLogConverter.categoryLogConverter(category));
    }

    @DeleteMapping("/{categoryId}")
    public void deleteCategory(@RequestBody Category category, @PathVariable("categoryId") int categoryId) {
        categoryService.deleteCategory(category);
        categoryLogService.insert(TheLogConverter.categoryLogConverter(category));
    }
}