package com.example.controller;

import com.example.entity.Product;
import com.example.entity.TheLogConverter;
import com.example.service.ProductLogService;
import com.example.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/categories/{categoryId}/products")
public class ProductController {

    @Autowired
    public ProductService productService;
    @Autowired
    private ProductLogService productLogService;

    @GetMapping
    public Iterable<Product> getAllProducts(@PathVariable("categoryId") Long categoryId) {
        return productService.findAll();
    }

    @GetMapping("/{productId}")
    public Optional<Product> searchProduct(@PathVariable("productId") int productId, @PathVariable("categoryId") Long categoryId) {
        return productService.findById(productId);
    }

    @PostMapping
    public void addProduct(@RequestBody Product product, @PathVariable("categoryId") Long categoryId) {
        productService.insert(product);
        productLogService.insert(TheLogConverter.productLogConverter(product));
    }

    @PutMapping("/{productId}")
    public void updateProduct(@RequestBody Product product, @PathVariable("productId") Long productId, @PathVariable("categoryId") Long categoryId) {
        productService.updateProduct(product);
        productLogService.insert(TheLogConverter.productLogConverter(product));
    }

    @DeleteMapping("/{productId}")
    public void deleteProduct(@RequestBody Product product, @PathVariable("productId") Long productId, @PathVariable("categoryId") Long categoryId) {
        productService.deleteProduct(product);
        productLogService.insert(TheLogConverter.productLogConverter(product));
    }
}