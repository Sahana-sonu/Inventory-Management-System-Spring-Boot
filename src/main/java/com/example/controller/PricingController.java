package com.example.controller;

import com.example.entity.Pricing;
import com.example.entity.TheLogConverter;
import com.example.service.PricingLogService;
import com.example.service.PricingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/categories/{categoryId}/products/{productId}/pricings")
public class PricingController {

    @Autowired
    public PricingService pricingService;
    @Autowired
    private PricingLogService pricingLogService;

    @GetMapping
    public Iterable<Pricing> getAllPricing(@PathVariable("categoryId") Long categoryId, @PathVariable("productId") Long productId) {
        // Your implementation
        return pricingService.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Pricing> searchPricing(@PathVariable("id") int id, @PathVariable("categoryId") Long categoryId, @PathVariable("productId") Long productId) {
        // Your implementation
        return pricingService.find(id);
    }

    @PostMapping
    public void addPricing(@RequestBody Pricing pricing, @PathVariable("categoryId") Long categoryId, @PathVariable("productId") Long productId) {
        // Your implementation
        pricingService.insert(pricing);
        pricingLogService.insert(TheLogConverter.pricingLogLogConverter(pricing));
    }

    @PutMapping("/{id}")
    public void updatePricing(@RequestBody Pricing pricing, @PathVariable("id") int id, @PathVariable("categoryId") Long categoryId, @PathVariable("productId") Long productId) {
        // Your implementation
        pricingService.updatePricing(pricing);
        pricingLogService.insert(TheLogConverter.pricingLogLogConverter(pricing));
    }

    @DeleteMapping("/{id}")
    public void deletePricing(@RequestBody Pricing pricing, @PathVariable("id") int id, @PathVariable("categoryId") Long categoryId, @PathVariable("productId") Long productId) {
        // Your implementation
        pricingService.deletePricing(pricing);
        pricingLogService.insert(TheLogConverter.pricingLogLogConverter(pricing));
    }
}